import java.util.Scanner;
public class Shop {
  public static void main(String[] args) {
    Scanner read = new Scanner(System.in);
    Humain[] x = new Humain[4];
    for(int i = 0; i < x.length; i++) {
      x[i] = new Humain(); 
      System.out.println("What is the name of your humain?");
      x[i].name = read.next();
      System.out.println("What is the ethnicity of your humain?");
      x[i].ethnicity = read.next();
      System.out.println("What is the age of your humain?");
      x[i].age = read.nextInt();
      System.out.println("What is the height of your humain?");
      x[i].height = read.nextDouble();
      System.out.println("What is the weight of your humain?");
      x[i].weight = read.nextDouble();
    }
    System.out.println(x[3].name);
    System.out.println(x[3].ethnicity);
    System.out.println(x[3].age);
    System.out.println(x[3].height);
    System.out.println(x[3].weight);
    
    x[3].growOlder(); 
  }
}