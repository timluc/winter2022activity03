public class Humain {
  public String name;
  public String ethnicity;
  public int age;
  public double height;
  public double weight;
 

  public void growOlder() {
    System.out.println("You have grown older. You are now "+(this.age + 1)+" year old!");
  }
}